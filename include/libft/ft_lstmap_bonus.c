/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 13:33:01 by tbrouill          #+#    #+#             */
/*   Updated: 2019/12/02 20:38:15 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*new_lst;
	t_list	*tmp;

	if (!lst)
		return (NULL);
	if (lst->next)
		ft_lstmap(lst->next, (*f), (*del));
	if (!(tmp = ft_lstnew((*f)(lst->content))))
		return (NULL);
	ft_lstadd_front(&new_lst, tmp);
	return (new_lst);
}
