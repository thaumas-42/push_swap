//
// Created by patrick on 17/03/2022.
//

#include "../push_swap.h"

void rotate_a_b(t_list *lista, t_list *listb)
{
	rotate_a(lista);
	rotate_b(listb);
}

void rotate_a(t_list *list)
{
	int *tmp;

	if (list->length < 2)
		return;
	if (!(tmp = ft_calloc((list->length), sizeof(int))))
		exit_error();
	tmp[list->length - 1] = list->list[0];
	ft_memmove(tmp, list->list + 1, (list->length - 1) * sizeof(int));
	free(list->list);
	list->list = tmp;
}

void rotate_b(t_list *list)
{
	int *tmp;

	if (list->length < 2)
		return;
	if (!(tmp = ft_calloc((list->length), sizeof(int))))
		exit_error();
	tmp[list->length - 1] = list->list[0];
	ft_memmove(tmp, list->list + 1, (list->length - 1) * sizeof(int));
	free(list->list);
	list->list = tmp;

}
