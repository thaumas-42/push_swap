//
// Created by patrick on 17/03/2022.
//

#include "../push_swap.h"

void swap_a(t_list *lista) {
	int i;
	int tmp;

	i = lista->length - 1;
	if (i >= 2) {
		tmp = lista->list[i];
		lista->list[i] = lista->list[i - 1];
		lista->list[i - 1] = tmp;
	}
}

void swap_b(t_list *listb) {
	int i;
	int tmp;

	i = listb->length - 1;
	if (i >= 2) {
		tmp = listb->list[i];
		listb->list[i] = listb->list[i - 1];
		listb->list[i - 1] = tmp;
	}
}

void swap_a_b(t_list *lista, t_list *listb) {
	swap_a(lista);
	swap_b(listb);
}