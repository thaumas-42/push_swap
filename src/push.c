//
// Created by patrick on 17/03/2022.
//

#include "../push_swap.h"

void push_a(t_list *lista, t_list *listb) {
	int *tmp;
	int *tmp2;

	if (listb->length == 0)
		return;
//	if (lista->length == 0 && !(lista->list = ft_calloc(1, sizeof(int))))
//		exit_error();
	if (!(tmp = ft_calloc(lista->length + 1, sizeof(int))))
		exit_error();
	ft_memcpy(tmp, lista->list, (lista->length) * sizeof(int));
	if (lista->length)
		free(lista->list);
	lista->list = tmp;
	listb->length--;
	lista->list[lista->length] = listb->list[listb->length];
	lista->length++;
	if (listb->length != 0) {
		if (!(tmp2 = ft_calloc(listb->length - 1, sizeof(int))))
			exit_error();
		ft_memcpy(tmp2, listb->list, (listb->length - 1) * sizeof(int));
		free(listb->list);
		listb->list = tmp2;
	} else {
		free(listb->list);
		listb->list = NULL;
	}
}

void push_b(t_list *lista, t_list *listb) {
	int *tmp;
	int *tmp2;

	if (lista->length == 0)
		return;
//	if (listb->length == 0 && !(listb->list = ft_calloc(1, sizeof(int))))
//		exit_error();
	if (!(tmp = ft_calloc(listb->length + 1, sizeof(int))))
		exit_error();
	ft_memcpy(tmp, listb->list, (listb->length) * sizeof(int));
	if (listb->length)
		free(listb->list);
	listb->list = tmp;
	lista->length--;
	listb->list[listb->length] = lista->list[lista->length];
	listb->length++;
	if (lista->length != 0) {
		if (!(tmp2 = ft_calloc(lista->length, sizeof(int))))
			exit_error();
		ft_memcpy(tmp2, lista->list, (lista->length) * sizeof(int));
		free(lista->list);
		lista->list = tmp2;
	} else {
		free(lista->list);
		lista->list = NULL;
	}
}