//
// Created by patrick on 17/03/2022.
//

#include "../push_swap.h"

void reverse_rotate_a_b(t_list *lista, t_list *listb) {
	reverse_rotate_a(lista);
	reverse_rotate_b(listb);
}

void reverse_rotate_a(t_list *list) { //TODO inversion rra ra
	int *tmp;

	if (list->length < 2)
		return;
	if (!(tmp = ft_calloc((list->length), sizeof(int))))
		exit_error();
	tmp[0] = list->list[list->length - 1];
	ft_memmove(tmp + 1, list->list, (list->length - 1) * sizeof(int));
	free(list->list);
	list->list = tmp;
}

void reverse_rotate_b(t_list *list) { // TODO inversion rrb ra
	int *tmp;

	if (list->length < 2)
		return;
	if (!(tmp = ft_calloc((list->length), sizeof(int))))
		exit_error();
	tmp[0] = list->list[list->length - 1];
	ft_memmove(tmp + 1, list->list, (list->length - 1) * sizeof(int));
	free(list->list);
	list->list = tmp;
}