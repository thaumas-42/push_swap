#include "../push_swap.h"
//REMEMBER STACK UP is list.list[list.length] and STACK BOTTOM is 0
int main(int argc, char **argv)
{
	t_list lista;
	t_list listb;

	lista = (t_list) {.list = ft_calloc((argc - 1), sizeof(int))};
	if (!lista.list || argc <= 1)
		exit_error();
	while (--argc)
		lista.list[lista.length++] = ft_atoi2(argv[argc]);
	lista.max_length = lista.length;
	check_double(lista);
	listb = (t_list) {.max_length = lista.max_length};
	short_list(&lista, &listb);
	if (lista.length)
		free(lista.list);
	if (listb.length)
		free(listb.list);
	return (0);
}

void check_double(t_list list)
{
	int i;
	int j;

	i = -1;
	while (++i < list.max_length) {
		j = -1;
		while (++j < i)
			if (list.list[i] == list.list[j])
				exit_error();
	}
}

int ft_atoi2(const char *str)
{
	int i;
	long result;

	result = 0;
	i = (str[0] == '-' || str[0] == '+');
	while (str[i]) {
		if (!(str[i] >= '0' && str[i] <= '9'))
			exit_error();
		result = result * 10 + (str[i] - '0');
		i++;
	}
	result = (str[0] == '-') ? result * -1 : result;
	if (result < -2147483648 || result > 2147483647)
		exit_error();
	return ((int) result);
}

void short_list(t_list *lista, t_list *listb)
{
	//TODO Algorithms and handlers implementation
	t_test_list test_list;

	test_list = (t_test_list) {};
	test_list.test1.lista = array_dup(lista);
	test_list.test1.listb = array_dup(listb);
	test_list.test1 = sorter_stupid_rra_pb(&test_list.test1.lista, &test_list.test1.listb);
	test_list.test2.lista = array_dup(lista);
	test_list.test2.listb = array_dup(listb);
	test_list.test2 = sorter_stupid_ra_rra_pb(&test_list.test2.lista, &test_list.test2.listb);
//	put_list(lista);
//	put_list(listb);
	if (test_list.test1.score < test_list.test2.score)
		ft_putstr_fd(test_list.test1.instructions, STDOUT_FILENO);
	else
		ft_putstr_fd(test_list.test2.instructions, STDOUT_FILENO);

	if (test_list.test1.instructions) {
		free(test_list.test1.instructions);
		if (test_list.test1.lista.list)
			free(test_list.test1.lista.list);
		if (test_list.test1.listb.list)
			free(test_list.test1.listb.list);
		test_list.test1.instructions = NULL;
		test_list.test1.lista.list = NULL;
		test_list.test1.listb.list = NULL;
	}
	if (test_list.test2.instructions) {
		free(test_list.test2.instructions);
		if (test_list.test2.lista.list)
			free(test_list.test2.lista.list);
		if (test_list.test2.listb.list)
			free(test_list.test2.listb.list);
		test_list.test2.instructions = NULL;
		test_list.test2.lista.list = NULL;
		test_list.test2.listb.list = NULL;
	}
}

t_list array_dup(t_list *list)
{
	t_list result;

	if (!(result.list = ft_calloc(list->length, sizeof(int))))
		exit_error();
	ft_memcpy(result.list, list->list, list->length * sizeof(int));
	result.length = list->length;
	result.max_length = list->max_length;
	return (result);
}

t_test sorter_stupid_rra_pb(t_list *lista, t_list *listb)
{
	t_test result;

	result = (t_test) {.lista = *lista, .listb = *listb};
	while (!short_check(lista) || lista->length < lista->max_length) {
		if (lista->length != 0) {
			if (is_smallest(lista, lista->list[lista->length - 1]))
//				do_pb(lista, listb, &result);
				do_operation(push_b, &result, "pb\n", 1);
			else
				do_operation(reverse_rotate_a, &result, "rra\n", 0);
//				do_rra(lista, &result);
		} else {
			while (listb->length) {
				// TODO en mode debug, ne rentre jamais ici
				do_operation(push_a, &result, "pa\n", 1);
				*lista = result.lista;
				*listb = result.listb;
			}
//				do_pa(lista, listb, &result);
			break;
		}
		*lista = result.lista;
		*listb = result.listb;
	}
	return (result);
}

t_test sorter_stupid_ra_rra_pb(t_list *lista, t_list *listb)
{
	t_test result;

	result = (t_test) {.lista = *lista, .listb = *listb};
	while (!short_check(lista) || lista->length < lista->max_length) {
		if (lista->length != 0) {
			if (is_smallest(lista, lista->list[lista->length - 1]))
				do_operation(push_b, &result, "pb\n", 1);
//				do_pb(lista, listb, &result);
			else if (smallest_pos(lista) >= lista->length / 2)
				do_operation(rotate_a, &result, "ra\n", 0);
//				do_ra(lista, &result);
			else
				do_operation(reverse_rotate_a, &result, "rra\n", 0);
//				do_rra(lista, &result);
		} else {
			while (listb->length) {
				do_operation(push_a, &result, "pa\n", 1);
				*lista = result.lista;
				*listb = result.listb;
			}
//				do_pa(lista, listb, &result);
			break;
		}
		*lista = result.lista;
		*listb = result.listb;
	}
	return (result);
}

int smallest_pos(t_list *lista)
{
	int i;
	int s_num;

	i = -1;
	s_num = 2147483647;
	while (++i < lista->length)
		if (lista->list[i] < s_num)
			s_num = lista->list[i];
	return (i);
}
//
//void do_ra(t_list *lista, t_test *report) {
//	char *tmp;
//	rotate_a(lista);
//	report->score++;
//	tmp = ft_strjoin_safe(report->instructions, "ra\n");
//	if (report->instructions)
//		free(report->instructions);
//	report->instructions = tmp;
//}
//
//void do_pa(t_list *lista, t_list *listb, t_test *report) {
//	char *tmp;
//	push_a(lista, listb);
//	report->score++;
//	tmp = ft_strjoin_safe(report->instructions, "pa\n");
//	if (report->instructions)
//		free(report->instructions);
//	report->instructions = tmp;
//}
//
//void do_rra(t_list *list, t_test *report) {
//	char *tmp;
//	reverse_rotate_a(list);
//	report->score++;
//	tmp = ft_strjoin_safe(report->instructions, "rra\n");
//	if (report->instructions)
//		free(report->instructions);
//	report->instructions = tmp;
//}
//
//void do_pb(t_list *lista, t_list *listb, t_test *report) {
//	char *tmp;
//	push_b(lista, listb);
//	report->score++;
//	tmp = ft_strjoin_safe(report->instructions, "pb\n");
//	if (report->instructions)
//		free(report->instructions);
//	report->instructions = tmp;
//}

int is_smallest(t_list *list, int num)
{
	int i;

	i = -1;
	while (++i < list->length)
		if (list->list[i] < num)
			return (0);
	return (1);
}

int short_check(t_list *list)
{
	int i;

	i = 0;
	while (++i < list->length)
		if (list->list[i] < list->list[i - 1])
			return (0);
	return (1);
}

char *ft_strjoin_safe(char *str1, const char *str2)
{
	char *dest;

	if (!(dest = ft_strjoin(str1, str2)))
		exit_error();
	return (dest);
}

void exit_error(void)
{
	ft_putendl_fd("Error", STDERR_FILENO);
	exit(1);
}

void put_list(const t_list *list)
{
	int i;
	char *tmp;

	i = list->length + 1;
	while (--i) {
		ft_putendl_fd((tmp = ft_itoa(list->list[i - 1])), STDOUT_FILENO);
		free(tmp);
	}
	ft_putchar_fd('\n', STDOUT_FILENO);
}

void do_operation(void (*op)(), t_test *test, const char *instructions, int listb)
{
	char *tmp;

	if (listb)
		op(&(test->lista), &(test->listb));
	else
		op(&(test->lista));
	test->score++;
	tmp = ft_strjoin_safe(test->instructions, instructions);
	if (test->instructions)
		free(test->instructions);
	test->instructions = tmp;
}
