//
//
// Created by patrick on 04/03/2022.
//

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdarg.h>
# include <stdio.h>
# include <stdlib.h>
# include "include/libft/libft.h"

typedef struct s_list
{
	int *list;
	int length;
	int max_length;
} t_list;

typedef struct s_test
{
	unsigned int score;
	char *instructions;
	t_list lista;
	t_list listb;
} t_test;

typedef struct s_test_list
{
	t_test test1;
	t_test test2;
	t_test test3;
} t_test_list;

void short_list(t_list *lista, t_list *listb);

int main(int argc, char **argv);

void exit_error(void);

void reverse_rotate_a_b(t_list *lista, t_list *listb);

void reverse_rotate_a(t_list *list);

void reverse_rotate_b(t_list *list);

void rotate_a_b(t_list *lista, t_list *listb);

void rotate_b(t_list *list);

void rotate_a(t_list *list);

void put_list(const t_list *list);

void swap_a(struct s_list *lista);

void swap_b(struct s_list *listb);

void swap_a_b(struct s_list *lista, struct s_list *listb);

void push_a(t_list *lista, t_list *listb);

void push_b(t_list *lista, t_list *listb);

t_test sorter_stupid_rra_pb(t_list *lista, t_list *listb);

char *ft_strjoin_safe(char *str1, const char *str2);

int short_check(t_list *list);

int is_smallest(t_list *list, int num);

void do_operation(void (*op)(), t_test *test, const char *instructions,
				  int listb);

int smallest_pos(t_list *lista);

int ft_atoi2(const char *str);

void check_double(t_list list);

t_test sorter_stupid_ra_rra_pb(t_list *lista, t_list *listb);

t_list array_dup(t_list *list);

#endif //PUSH_SWAP_H
//void do_rra(t_list *list, t_test *report);

//void do_pa(t_list *lista, t_list *listb, t_test *report);

//void do_pb(t_list *lista, t_list *listb, t_test *report);
//
//void do_ra(t_list *lista, t_test *result);
