SRC_FOLDER = src
SRC := $(SRC_FOLDER)/push_swap.c \
		$(SRC_FOLDER)/rotate.c \
		$(SRC_FOLDER)/reverse_rotate.c \
		$(SRC_FOLDER)/push.c \
		$(SRC_FOLDER)/swap.c
OBJ = $(SRC:.c=.o)
NAME := push_swap
LIBFT_PATH = include/libft
CC = clang
C_FLAGS = -Wall -Wextra -Werror
LINKER = clang
L_FLAGS =  -L$(LIBFT_PATH) -lft
RM = rm -rf

.c.o:
			$(CC) $(CFLAGS) -g -O0 -c $< -o $(<:.c=.o)

$(NAME):	libft.a $(OBJ)
			$(LINKER) $(OBJ) $(L_FLAGS) -g -O0 -o $(NAME)

libft.a:
			cd $(LIBFT_PATH) && make all

all:		$(NAME)

clean:
			$(RM) $(OBJ)
			cd $(LIBFT_PATH) && make clean

fclean:		clean
			$(RM) $(NAME)
			cd $(LIBFT_PATH) && make fclean

re:			fclean all

.PHONY:		all fclean clean re
